"use strict";

var babelify = require('babelify'),
  browserify = require('browserify'),
  concat = require('gulp-concat'),
  cssmin = require('gulp-cssmin'),
  del = require('del'),
  env = require('gulp-env'),
  gulp = require('gulp'),
  gulpif = require('gulp-if'),
  gutil = require('gulp-util'),
  notify = require('gulp-notify'),
  runSequence = require('run-sequence'),
  sass = require('gulp-sass'),
  source = require('vinyl-source-stream'),
  streamify = require('gulp-streamify'),
  uglify = require('gulp-uglify'),
  watchify = require('watchify');

function logError(errorMessage) {
  gutil.log(
    gutil.colors.red('Error: ') + gutil.colors.yellow(errorMessage)
  );
}

gulp.task('bundleJS', function(cb) {
  var appBundler = browserify({
      entries: ['./src/js/App.js'],
      transform: [babelify.configure({
                  presets: ["es2015"],
                  plugins: ["transform-decorators-legacy"]
                 })],
      debug: false,
      cache: {}, packageCache: {}, fullPaths: true
  });

  var rebundle = function(_cb) {
    console.log(process.env.dev === 'false')
    return function() {
      var start = Date.now();
      appBundler.bundle()
      .on('error', function(error) {
        logError(error.message);
        var filename = './build/App.js';
        logError('Removing ' + filename);
        del(filename);
        this.emit('end');
      })
      .pipe(source('App.js'))
      .pipe(gulpif(process.env.dev === 'false', streamify(uglify())))
      .pipe(gulp.dest('./build'))
      .pipe(notify(function () {
        gutil.log('bundleJS built in ' + (Date.now() - start) + 'ms');
        _cb();
      }));
    }
  };

  rebundle(cb)();

  if (process.env.watch === 'true') {
    watchify(appBundler).on('update', rebundle(function(){}));
  }
});

gulp.task('compileScss', function() {
  var start = Date.now();
  return gulp.on('error', gutil.log)
    .src('./src/scss/*.{scss,sass,css}')
    .pipe(sass()
      .on('error', function(error) {
        logError(error.message);
        this.emit('end');
      })
    )
    .pipe(concat('main.css'))
    .pipe(cssmin())
    .pipe(gulp.dest('./build'))
    .pipe(notify({onLast: true, message: function(options) {
      gutil.log('SCSS compiled in ' + (Date.now() - start) + 'ms');
    }}));
});

gulp.task('processHTML', function() {
  var start = Date.now();
  return gulp.on('error', function(err) {
      logError(err);
      this.emit('end')
    })
    .src('./src/html/**/*.html')
    .pipe(gulp.dest('./build'))
    .pipe(notify({
      onLast: true,
      notifier: function(options, callback) {
        gutil.log('HTML files built in ' + (Date.now() - start) + 'ms')
      }
    }));
});

gulp.task('watch', function() {
  // these variables will turn into strings
  env.set({
    //watch bundleJS from within bundleJS for better rebundling speed
    watch: true,
    dev: true
  });

  runSequence([
    'processHTML',
    'bundleJS',
    'compileScss'
  ]);

  gulp.watch(
    ['./src/html/**/*.html'],
    ['processHTML']
  );

  gulp.watch(
    ['./src/scss/*.{scss,sass,css}'],
    ['compileScss']
  );
});

gulp.task('default', function() {
  env.set({
    watch: false,
    dev: false
  });

  runSequence([
    'processHTML',
    'bundleJS',
    'compileScss'
  ]);
});
export const Keys = {
  UP: 38,
  DOWN: 40,
  LEFT: 37,
  RIGHT: 39,
  ESCAPE: 27,
  SPACEBAR: 32
}

var tiles = {
  SNAKE_BODY: 0,
  SNAKE_HEAD: 1,
  CLEAR: 2
}

export const Tiles = tiles;

var tileColors = {};
tileColors[tiles.SNAKE_BODY] = 'rgba(128, 255, 128, .7)';
tileColors[tiles.SNAKE_HEAD] = 'rgba(255, 128, 128, .7)';
tileColors[tiles.CLEAR] = 'rgba(128, 128, 128, .1)';

export const TileColors = tileColors;

export const Directions = {
  LEFT: 0,
  RIGHT: 1,
  UP: 2,
  DOWN: 3
}
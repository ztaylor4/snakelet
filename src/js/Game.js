import settings from './Settings';
import { Keys, Tiles, Directions } from './Constants';


export class Game {
  constructor(board, targets) {
    this.width = board.dimensions.x;
    this.height = board.dimensions.y;
    this.occupied = new Array(this.width);
    for (var i = 0; i < this.width; i++) {
      this.occupied[i] = new Array(this.height);
      for (var j = 0; j < this.height; j++) {
        this.occupied[i][j] = false;
      }
    }

    this.snakeBody = [{
      x: Math.floor(board.dimensions.x/2)-3,
      y: Math.floor(board.dimensions.y/2)
    }];
    this.occupied[this.snakeBody[0].x][this.snakeBody[0].y] = true;

    this.currentDirection = Directions.RIGHT;

    this.tailDirection = null; // direction of the tail from the head.  The head can't go that direction.

    this.snakeMaxLength = settings.INITIAL_SNAKE_LENGTH;

    this.targetSquare = null;
    this.targets = targets;

    this.currentTarget = this.targets.getRandomTarget(this.snakeBody);
    if (this.currentTarget !== null) {
      this.currentTarget.activate();
    }
  }

  registerInput(key) {
    var newDirection;
    if (key == Keys.LEFT) {
      newDirection = Directions.LEFT;
    } else if (key == Keys.RIGHT) {
      newDirection = Directions.RIGHT;
    } else if (key == Keys.DOWN) {
      newDirection = Directions.DOWN;
    } else if (key == Keys.UP) {
      newDirection = Directions.UP;
    }

    if (newDirection != this.tailDirection || this.tailDirection === null) {
      this.currentDirection = newDirection;
    }
  }

  advanceSnake() {
    var headSquare = this.snakeBody[0];

    var nextLoc = {
      x: headSquare.x,
      y: headSquare.y
    };
    if (this.currentDirection == Directions.LEFT) {
      nextLoc.x = headSquare.x - 1;
      this.tailDirection = Directions.RIGHT;
    } else if (this.currentDirection == Directions.RIGHT) {
      nextLoc.x = headSquare.x + 1;
      this.tailDirection = Directions.LEFT;
    } else if (this.currentDirection == Directions.UP) {
      nextLoc.y = headSquare.y - 1;
      this.tailDirection = Directions.DOWN;
    } else if (this.currentDirection == Directions.DOWN) {
      nextLoc.y = headSquare.y + 1;
      this.tailDirection = Directions.UP;
    }

    // end if out of bounds
    if (nextLoc.x < 0 || nextLoc.x >= this.width || nextLoc.y < 0 || nextLoc.y >= this.height) {
      if (this.currentTarget !== null) {
        this.currentTarget.deactivate();
      }
      return false;
    }

    // end if colliding with self
    if (this.occupied[nextLoc.x][nextLoc.y]) {
      if (this.currentTarget !== null) {
        this.currentTarget.deactivate();
      }
      return false;
    }

    this.occupied[nextLoc.x][nextLoc.y] = true;
    this.snakeBody.unshift(nextLoc);

    // get a new target if hitting the old one
    if (this.currentTarget !== null && this.currentTarget.checkCollision(nextLoc.x, nextLoc.y)) {
      this.currentTarget.disappear();
      this.currentTarget = null;
      this.snakeMaxLength++;
    }

    if (this.snakeBody.length > this.snakeMaxLength) {
      var oldSegment = this.snakeBody.pop();
      this.occupied[oldSegment.x][oldSegment.y] = false;
    }

    // if the snake is covering a lot of words, a target may not be available until the next frame
    if (this.currentTarget === null) {
      this.currentTarget = this.targets.getRandomTarget(this.snakeBody);
      if (this.currentTarget !== null) {
        this.currentTarget.activate();
      }
    }

    return true;
  }
}
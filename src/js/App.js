import settings from './Settings';
import { Board } from './Board';
import { Snake } from './Snake';
import { Keys } from './Constants';


var snake = new Snake();
snake.drawBorder();

var resizeTimer;
window.addEventListener("resize", resize);

// Handle resizing.  End the game if in progress, and redraw the border.
// The resizeTimer prevents redrawing constantly.
function resize() {
  clearTimeout(resizeTimer);
  resizeTimer = setTimeout(function() {
    if (snake.isPlaying()) {
      snake.stopGame();
    }
    snake.drawBorder();
  }, 250);
}

// Establish mouse and keyboard listeners for starting and stopping the game.
document.getElementById("play-button").addEventListener("click", startGame);
document.addEventListener('keydown', function(event) {
  if(snake.isPlaying() && event.keyCode === Keys.ESCAPE) {
    event.preventDefault();
    snake.stopGame();
  }
  if(event.keyCode === Keys.SPACEBAR) {
    event.preventDefault();
    if(!snake.isPlaying()) {
      snake.startGame();
    }
  }
});

function startGame() {
  if (!snake.isPlaying()) {
    snake.startGame();
  }
}

import autobind from 'autobind-decorator';
import settings from './Settings';

import { Board } from './Board';
import { Game } from './Game';
import { Keys } from './Constants';
import { Targets } from './Targets';


export class Snake {
  constructor() {
    this.activeGame = false;
    this.board = new Board(); // Snakelet View
    this.game; // Snakelet Logic Model
    this.targets = new Targets(document.getElementsByClassName(settings.TARGET_CLASS));
  }

  @autobind
  keyListener(event) {
    if([Keys.UP, Keys.DOWN, Keys.LEFT, Keys.RIGHT].indexOf(event.keyCode) != -1) {
      event.preventDefault();
      this.game.registerInput(event.keyCode);
    } else {
      return;
    }
  }

  isPlaying() {
    return this.activeGame;
  }

  drawBorder() {
    // add a border to the page so that the Snakelet board will be a multiple of tile size
    this.board.drawBorder();
  }

  @autobind
  renderFrame() {
    if (!this.game.advanceSnake()) {
      this.gameOver();
    }
    if (this.isPlaying()) {
      this.board.drawBoard(this.game);
    }
  }

  disablePageMovement() {
    // more could be done to disable scrolling
    document.addEventListener('mousewheel', function(event) {
      event.preventDefault();
    });
    document.getElementById(settings.OVERLAY_ID).style['pointer-events'] = 'auto';
  }

  enablePageMovement() {
    document.removeEventListener('mousewheel', function(event) {
      event.preventDefault();
    });
    document.getElementById(settings.OVERLAY_ID).style['pointer-events'] = 'none';
  }

  startGame() {
    this.activeGame = true;
    console.log("Game started")

    document.getElementById("play-button").classList.add('disabled');
    document.getElementById("play-button").innerHTML = 'Press ESC to stop';
    this.board.measureBoard();

    this.targets.calibrateLocations(this.board);
    this.game = new Game(this.board, this.targets);

    document.addEventListener('keydown', this.keyListener);
    this.disablePageMovement();

    this.renderInterval = setInterval(this.renderFrame, settings.FRAME_LENGTH);
    this.renderFrame();
  }

  stopGame() {
    console.log("Game stopped");
    document.getElementById("play-button").classList.remove('disabled');
    document.getElementById("play-button").innerHTML = 'Play Snakelet';

    document.removeEventListener('keydown', this.keyListener);
    this.enablePageMovement();

    this.targets.deactivateTargets();
    this.board.clearBoard();
    clearInterval(this.renderInterval);
    this.activeGame = false;
  }

  gameOver() {
    console.log('Game over');
    this.stopGame();
    if (this.targets.allEaten()) {
      console.log("You won the game")
      this.targets.resetTargets();
    }
  }
}
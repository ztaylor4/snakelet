import autobind from 'autobind-decorator';
import settings from './Settings';


class Target {
    constructor(element) {
      this.element = element;
      this.squares = [];
      this.eaten = false;
      this.outOfBounds = false;
      this.activated = false;
    }

    // These animation methods should be generalized and moved elsewhere
    animateToTargetOpacity(element, start, target, steps, speed, cb) {
      var op = start;
      var step = (op - target)/steps;
      var id = setInterval(inc, speed);
      var that = this;

      function inc() {
        if (!that.activated) {
          clearInterval(id);
          element.style.opacity = that.eaten ? 0 : 1;
        } else if (op == target) {
          clearInterval(id);
          cb();
        } else {
          op -= step;
          element.style.opacity = op/100.;
        }
      }
    }

    @autobind
    animateDown() {
      if (this.activated || !this.eaten) {
        this.animateToTargetOpacity(this.element, 0, 100, 50, 15, this.animateUp);
      }
    }

    @autobind
    animateUp() {
      if (this.activated) {
        this.animateToTargetOpacity(this.element, 100, 0, 50, 15, this.animateDown);
      }
    }

    // For when a target is the active one for the worm.
    activate() {
      this.element.classList.add('notice');
      this.activated = true;
      this.animateUp();
    }

    // For after being eaten (should also disappear) or after game over (shouldn't disappear)
    deactivate() {
      this.element.classList.remove('notice');
      this.activated = false;
    }

    // For when a target is eaten by the worm
    disappear() {
      this.element.style.opacity = 0;
      this.eaten = true;
      this.deactivate();
    }

    // Reviving the target, e.g. to reset the game
    reappear() {
      this.element.style.opacity = 1;
      this.eaten = false;
    }

    // Match targets with their squares in the game model.  Only include squares that would be in the displayed canvas.
    findLocation(dimensions) {
      this.width = this.element.offsetWidth;
      this.height = this.element.offsetHeight;
      this.x = this.element.offsetLeft;
      this.y = this.element.offsetTop;


      var lowX = Math.floor((this.x - dimensions.leftBorder) / settings.GRID_SIZE);
      var highX = Math.floor((this.x + this.width - dimensions.leftBorder) / settings.GRID_SIZE);
      var lowY = Math.floor((this.y - dimensions.topBorder) / settings.GRID_SIZE);
      var highY = Math.floor((this.y + this.height - dimensions.topBorder) / settings.GRID_SIZE);

      this.squares = [];
      for (var x = lowX; x <= highX; x++) {
        for (var y = lowY; y <= highY; y++) {
          if (y >= 0 && y < dimensions.y) {
            this.squares.push({
              x: x,
              y: y
            });
          }
        }
      }
    }

    checkCollision(x, y) {
      for (var i = 0; i < this.squares.length; i++) {
        if (x == this.squares[i].x && y == this.squares[i].y) {
          return true;
        }
      }
      return false;
    }

    hasSnakeCollision(snakeBody) {
      for (var i = 0; i < snakeBody.length; i++) {
        if (this.checkCollision(snakeBody[i].x, snakeBody[i].y)) {
          return true;
        }
      }
      return false;
    }
}

export class Targets {
  // input should be a list of elements
  constructor(elems) {
    this.targets = [];

    for (var i = 0; i < elems.length; i++) {
      var el = elems[i];
      var newEls = [];

      // break the elements apart, turn text nodes (nodeType == 3) into targets spans
      for (var j = 0; j < el.childNodes.length; j++) {
        var cn = el.childNodes[j];
        if (cn.nodeType == 3) {
          var tArr = cn.data.split(" ");
          for (var t in tArr) {
            var tEl = document.createElement("span");
            tEl.innerHTML = tArr[t];
            if(tEl.innerHTML.length > 0) {
              tEl.classList.add('target-unit');
              this.targets.push(new Target(tEl));
            }
            newEls.push(tEl);
          }
        } else {
          newEls.push(cn);
        }
      }

      el.innerHTML = "";
      el.style.margin = '-2px'; // adjustment since spans have slightly different padding
      for(var j = 0; j < newEls.length; j++) {
        el.appendChild(newEls[j]);
      }
    }
  }

  // Choose a random target.  If that target isn't available, try the next target.
  // If no targets are available, return null and let the worm proceed without a target.
  getRandomTarget(snakeBody) {
    if (this.targets.length == 0) {
      return null;
    }
    var targetIdx = Math.floor(Math.random() * this.targets.length);
    var firstAttempt = targetIdx;

    while (true) {
      if (this.targets[targetIdx].hasSnakeCollision(snakeBody) || this.targets[targetIdx].eaten || this.targets[targetIdx].squares.length == 0) {
        targetIdx = (targetIdx + 1) % this.targets.length;
        if (targetIdx == firstAttempt) {
          return null;
        }
      } else {
        return this.targets[targetIdx];
      }
    }
  }

  calibrateLocations(board) {
    for (var i = 0; i < this.targets.length; i++) {
      this.targets[i].findLocation(board.dimensions);
    }
  }

  // Check if all of the targets have been eaten (win condition)
  allEaten() {
    for (var i = 0; i < this.targets.length; i++) {
      if (!this.targets[i].eaten) {
        return false;
      }
    }
    return true;
  }

  resetTargets() {
    for (var i = 0; i < this.targets.length; i++) {
      this.targets[i].reappear();
    }
  }

  deactivateTargets() {
    for (var i = 0; i < this.targets.length; i++) {
      this.targets[i].deactivate();
    }
  }
}

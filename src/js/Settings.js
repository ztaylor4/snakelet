export default {
  GRID_SIZE: 60,
  DEBUG: false,
  DRAW_GRID: false,
  FRAME_LENGTH: 300,
  INITIAL_SNAKE_LENGTH: 2,
  OVERLAY_ID: 'snake-overlay',
  CANVAS_ID: 'snake-canvas',
  TARGET_CLASS: 'snake-target'
}

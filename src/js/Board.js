import settings from './Settings';
import { TileColors, Tiles } from './Constants';


export class Board {
  constructor() {
    this.displayGrid = false;
    this.dimensions = {};
  }

  // Calculate a border that puts the page content in an area that evenly divides by the grid size.
  measureBoard() {
    var xPixels = window.innerWidth - window.innerWidth%settings.GRID_SIZE;
    var yPixels = window.innerHeight - window.innerHeight%settings.GRID_SIZE;
    this.dimensions = {
      x: xPixels / settings.GRID_SIZE,
      y: yPixels / settings.GRID_SIZE,
      xPixels: xPixels,
      yPixels: yPixels,
    }
    this.dimensions.leftBorder = (window.innerWidth - this.dimensions.xPixels)/2
    this.dimensions.topBorder = document.body.scrollTop + (window.innerHeight - this.dimensions.yPixels)/2
  }

  // Draw the border overlay over the page.  The border overlay also prevents mouse events during the game.
  // Initially this border was visible, thus its name, although now it's mainly used an invisible overlay.
  // The function of that border is now handled by adjusting the padding of the body.
  drawBorder() {
    this.measureBoard();

    var border = document.getElementById(settings.OVERLAY_ID);

    if (border === null) {
      border = document.createElement('div');
      border.setAttribute('id', settings.OVERLAY_ID);
    }

    border.classList.add('border-overlay');
    border.style.width = `${window.innerWidth}px`;
    border.style.height = `${window.innerHeight}px`;

    border.style['border-width'] = `${this.dimensions.topBorder}px ${this.dimensions.leftBorder}px`;

    document.body.appendChild(border);
    document.body.style.padding = `${this.dimensions.topBorder}px ${this.dimensions.leftBorder}px`;
  }

  // Draw a canvas that covers the board within the border.  Draw squres that represent the snake.
  drawBoard(game) {
    var canvas = document.getElementById(settings.CANVAS_ID);
    if (canvas === null) {
      canvas = document.createElement('canvas');
      canvas.setAttribute('id', settings.CANVAS_ID);
      canvas.style.position = 'absolute';
      canvas.style.top = `${this.dimensions.topBorder}px`;
      canvas.style.left = `${this.dimensions.leftBorder}px`;
      canvas.width = this.dimensions.xPixels;
      canvas.height = this.dimensions.yPixels;
      document.body.append(canvas);
    }

    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.stroke();

    ctx.fillStyle = TileColors[Tiles.SNAKE_HEAD];
    ctx.fillRect(game.snakeBody[0].x * settings.GRID_SIZE, game.snakeBody[0].y * settings.GRID_SIZE, settings.GRID_SIZE, settings.GRID_SIZE);

    for (var i = 1; i < game.snakeBody.length; i++) {
      ctx.fillStyle = TileColors[Tiles.SNAKE_BODY];
      ctx.fillRect(game.snakeBody[i].x * settings.GRID_SIZE, game.snakeBody[i].y * settings.GRID_SIZE, settings.GRID_SIZE, settings.GRID_SIZE);
    }

    if (settings.DRAW_GRID) {
      for (var x = 0; x < this.dimensions.x; x++) {
        for (var y = 0; y < this.dimensions.y; y++) {
          ctx.rect(x * settings.GRID_SIZE, y * settings.GRID_SIZE, settings.GRID_SIZE, settings.GRID_SIZE);
        }
      }
    }
    ctx.stroke();
  }

  clearBoard() {
    document.getElementById(settings.CANVAS_ID).remove()
  }
}
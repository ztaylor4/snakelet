## Snakelet

For a live demo of this project, go to [http://snakelet.zachtayloriv.net](http://snakelet.zachtayloriv.net).  Snakelet is an implementation of Snake that uses the words on a page the snake's food.  Snakelet breaks divs with a given class into multiple elements to find where those elements are on the page, then draws a snake in a separate canvas.

If the player runs the snake's head into a wall or into its own body, the game ends, but it can be resumed with the same words eaten by starting a new game.  Currently there is no win notification.  If a player eats all the words within the game grid, the snake continues to move with no new targets.  If the player loses in this condition, the page is reset.

If the only available words remaining are underneath the snake, the game will wait until the player moves off of the words to make them into targets.  As a result, there may be times in a game when there are temporarily no targets.


## Build instructions

The following will install the necessary packages and build the project.

    npm install
    gulp

In addition to gulp's default task, which builds the project once in a `./build` directory, gulp can be run to watch for changes and rebuild to that directory:

    gulp watch

There are some minor optimizations that could be made to the gulp task if the project were to grow, but these optimizations aren't necessary for this small project.

`gulp watch` and `gulp` both call tasks for processing javascript, css, and html.

The gulp task for javascript uses babel for ES6 transpiling (and the ES7 decorators), and bundles the javascript files into a single App.js file.  If the gulp process is run with its dev flag set (changed in the gulp task or run via `gulp watch`), the resulting bundle isn't uglified/minified and includes a sourcemap.

The gulp task for css performs a sass transformation, concatenates files, and minimizes the result.  The concatenation isn't necessary since this project has only one scss file, but it could be later on.

The gulp task for html currently just moves the index.html file to the build directory.


## To do

There are a few things I wish I had a chance to do in the time allotted.

 - Game over animation when players lose
 - Win notification when players win
 - Possibly more styling on the snake

However, the main modifications I would want to make to the program moving forward would generalize it to work on other webpages.  To do this, the program needs a lot of work related to text parsing and style maintenance.

For that modification, I also would probably keep a version of the webpage in the DOM in addition to the targetized version, for greater fidelity when users return from the game.

In addition, I'd like to add some front-end tests for the game and presentation logic.  However, since much of the logic that seemed most well-suited to testing was dealing with page renders (rather than easier to test logic like form submission or API results), I didn't have time to add any automated testing.

The comments throughout were also somewhat rushed and could stand to be cleaned up.  They also note some areas that could use refactoring, in particular separating out the animation methods.


## Javascript organization

App.js
: The main program file.  Starts and stops the game from page input, and monitors for resizing.  Adds keyboard and mouse listeners.

Board.js
: Generates the view of the game board (the snake and the grid on which it travels).  Measures the window to determine board size and draws the state of the game.  This used to draw a border around the page to shift the page contents into a smaller box that is an even multiple of the grid size.  Now that's done with padding, so many of the references to the 'border' could be factored out in a later version.

Constants.js
: Enums, keyboard constants, and colors for the snake.  The colors would probably make more logical sense in settings, a stylesheet, or a separate file.

Snake.js
: Keep track of the game and serve as a controller between the Board view and the Game model.  Also manages the refresh rate, captures or diverts key and  mouse events, prevents page movement during the game, and includes a game over function that could be expanded to connect to an animation or some other action.

Game.js
: The model of the logic of the game.  Checks for food consumption and loss conditions, and generates the next state of the game.

Settings.js
: A few adjustable parameters, including game speed, grid size, grid display, and a few id names used by the program.

Targets.js
: Creates targets from divs with a particular class.  This could be extended to create targets from other pages.  Also animates active food targets.  Animation for this should logically go into some sort of animation module.